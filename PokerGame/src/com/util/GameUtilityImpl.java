package com.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeMap;

import com.bean.Card;
import com.bean.Deck;
import com.bean.Player;
import com.bean.Rank;
import com.bean.Suit;

//Override and implement all the methods of GameUtility Interface here 
public class GameUtilityImpl implements GameUtility {

	@Override
	public Deck getCardDeck() {
		Set<Card> cards = new HashSet<Card>();
		Card card;
		for (Suit suit : Suit.values()) {
			for (Rank rank : Rank.values()) {
				card = new Card(rank, suit);
				cards.add(card);
			}
		}

		return new Deck(cards);
	}

	@Override
	public Map<Player, Set<Card>> distributeCards(Deck deck, int noOfPlayers, int noOfCards) {
		Map<Player, Set<Card>> distributedCards = new HashMap<Player, Set<Card>>();
		Set<Card> playerCards;
		List<Card> listOfCards = new ArrayList<Card>();
		listOfCards.addAll(deck.getCards());
		for (int i = 1; i <= noOfPlayers; i++) {
			playerCards = new HashSet<Card>();
			Collections.shuffle(listOfCards);
			System.out.println("Player" + i + "Cards are \t");
			for (int j = 0; j < noOfCards; j++) {
				playerCards.add(listOfCards.get(j));
				System.out.println(listOfCards.get(j).getRank() + "" + listOfCards.get(j).getSuit());
			}
			distributedCards.put(Player.getKey(i), playerCards);
			System.out.println("\n");
			listOfCards.removeAll(playerCards);
			playerCards = null;
		}
		return distributedCards;
	}

	@Override
	public Map<Player, Integer> playCards(Map<Player, Set<Card>> cardsMap) {
		Map<Player, Set<Card>> playerCards = cardsMap;
		Map<Player, Integer> playerRanks = new HashMap<Player, Integer>();
		int playerScore = 0;

		for (Map.Entry<Player, Set<Card>> eachPlayerCards : playerCards.entrySet()) {

			for (Card card : eachPlayerCards.getValue()) {
				playerScore += card.getRank().getValue();
			}
			playerRanks.put(eachPlayerCards.getKey(), playerScore);
			playerScore = 0;
		}
		return playerRanks;
	}

	@Override
	public Set<Player> declareWinner(Map<Player, Integer> result) {
		int maxScore = 0;
		Set<Player> winners = new HashSet<Player>();
		for (Map.Entry<Player, Integer> playerScore : result.entrySet()) {
			if (playerScore.getValue() > maxScore)
				maxScore = playerScore.getValue();
		}
		for (Map.Entry<Player, Integer> playerScore : result.entrySet()) {
			if (playerScore.getValue() == maxScore) {
				winners.add(playerScore.getKey());
			}
		}
		return winners;
	}

}
