package com.main;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.bean.Card;
import com.bean.Deck;
import com.bean.Player;
import com.util.GameUtilityImpl;

public class Client {

	public static void main(String[] args) {
		GameUtilityImpl impl = new  GameUtilityImpl();
		Deck deck = impl.getCardDeck();
		
		Map<Player, Set<Card>> distributedCards = impl.distributeCards(deck, 5, 4);
		Map<Player, Integer> playerScores = impl.playCards(distributedCards);
		Set<Player> winners = impl.declareWinner(playerScores);
		System.out.println("Winners are:\t");
		for (Player player : winners) {
			System.out.println(player.getPlayerNumber());
		}
	}
}






