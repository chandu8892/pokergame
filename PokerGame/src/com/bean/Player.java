package com.bean;

import java.util.HashMap;
import java.util.Map;

public enum Player {
	PLAYER_1(1),
	PLAYER_2(2),
	PLAYER_3(3),
	PLAYER_4(4),
	PLAYER_5(5);

	private int playerNumber;
	private static final Map<Integer, Player> lookup = new HashMap<Integer, Player>();
	
	static {
        for (Player p : Player.values()) {
            lookup.put(p.getPlayerNumber(), p);
        }
    }

	Player(int playerNumber) {
		this.playerNumber = playerNumber;
	}

	public int getPlayerNumber() {
		return playerNumber;
	}
	
	public static Player getKey(int abbreviation) {
        return lookup.get(abbreviation);
    }
	
}
