package com.bean;

import java.util.Set;

public class Deck {
	private Set<Card> cards;

	public Deck(Set<Card> cards) {
		super();
		this.cards = cards;
	}

	public Set<Card> getCards() {
		return cards;
	}

	public void setCards(Set<Card> cards) {
		this.cards = cards;
	}
}
